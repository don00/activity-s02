
package com.zuitt.activity;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Arrays;


public class S2A2 {
    public static void main(String [] args){
        Scanner scanner  = new Scanner(System.in);
        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;
        
        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime numer is: " + primeNumbers[4]);
        
        ArrayList<String> stringArray = new ArrayList<String>();
        
        stringArray.add("John");
        stringArray.add("Joe");
        stringArray.add("Jane");
        stringArray.add("Jenkins");
        System.out.println(stringArray);
        
        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
        
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush" , 20);
        inventory.put("soap", 12);
        
        System.out.println("Our current inventory consists of: " + inventory);
        
    }
}

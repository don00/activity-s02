
package com.zuitt.activity;
import java.util.Scanner;

public class S2A1 {
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Input year to be checked if a leap year. ");
        int input = scanner.nextInt();
        
        if((input % 100 != 0 && input % 4 == 0 ) 
                || input % 400 == 0 ){
            System.out.println(input + " is a leap year.");
        }else {
            System.out.println(input + " is NOT a leap year.");
        }

        
    }
}
